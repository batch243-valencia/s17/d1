// console.log("Hello Elliot");


// [Section] Function
    // Function
        // Function in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called or invoke.
        // Function to perform a certain task when called or invoke.
        // They are also used to prevent repeating lines/blocks of codes that perform the same task or function
    
    // Function declaration
        // (function statements) - defines a function with specified parameters.

        // Syntax:
        /*
            function functionName() {
                code block (statements);
            }
        */

        // function keyword - used to define a javascript functions
        // functionName - the function name. Funcction are named to be able to use later in the code.
        // function block ({}) - the statements which comprise the body of the function. this is where the code will be executed.

        function printName (){
            console.log("My name is Elliot");
            console.log("My last name is Timmy");
        }

        // Function Invocation
            // The code block and statements inside a function is not immediately executed when the function is defined/declared.
            // The code block and statements inside a function is executed when the function is invoked.
            // Its is common to use the term "CALL a function" instead of "invoke a Function"
        
        //  Let's invoke the functio that we declated

        // Syntax:
        /*
        functionName();
        */
        printName();

// [Sections] Function Declaration and Function Expression
        // function declaration
            // a function can be created through function declaration by using the keyword function and adding function name.

            // Declared functions are not excecuted immediately.

            declaredFunction();

            function declaredFunction(){
                console.log("Hellow World from the declaredFunction");
            }

            const consantFunc = function(){
                console.log("Initialized with const!");
            }
            consantFunc();

	// function expression
		// a function can also be stored in a variable. This is called function expression.

		// Anonymous function - functions without a name.

		/*variableFunction();*/

		let variableFunction = function(){
			console.log("Hello from variableFunction!");
		}

		variableFunction();

		let funcExpression = function funcName(){
			console.log("Hello from funcExpression!");
		}

		funcExpression();

	// You can reassign declared functions and function expression to new anonymous function.

		declaredFunction = function(){
			console.log("updated declaredFunction");
		}

		declaredFunction();


		funcExpression = function(){
			console.log("updated funcExpression");
		}

		funcExpression();

		// Function expression using const keyword
		const constantFunc = function(){
			console.log("Initialized with const!")
		}

		constantFunc();

		/*constantFunc = function(){
			console.log("Cannot be reaassigned!")
		}
		constantFunc();*/



// [Section] Function Scoping
        /*
        Scipt is accessibility of varaible within our program.
        
        Javascript Variable, it has 3 type of scope:
        1. Local/block scope
        2. Global scope
        3. Function scope
        */
       {
        // local/block
        let localVar = "Armando Perez";
        console.log (localVar);
       }
       //Global
       let globalVar = "Mr. Worldwide";
       console.log (globalVar);

    // Function Scope
    // Javascript has function scope: Each Scope creates a new block/scope.
    // Varuables defined inside a function are not accessible outside the function.
    
    function showNames(){
        let functionLet= "Jane";
        const functionConst ="John";
        console.log(functionLet);
        console.log(functionConst);
    }
    showNames();


    // the Variables functionLet, functionConst, are function scope and cabbit be accessed outside of the function that they were declared.
        // console.log(functionLet);  //ERROR
        // console.log(functionConst); // ERROR
    
//[Section] Nested Functions
        // you can create another inside a function
        function myNewfunction(){
            let name= "Jane";
            console.log(name);
            let nestFunction = function(){
                let nestedName = "John";
                console.log(nestedName);
                console.log(name);
            }
            nestFunction();
        }
        myNewfunction();

        // Function and Global Scope Variables
            let globalName = "Alexandro";
            function myNewFunction2(){
                let nameInside = "Renz";
                console.log(globalName);
                console.log(nameInside);
                // myNewFunction2(); loop haha
            }
            myNewFunction2();
// [Section] Using Alert
    // alert() allows us to show a small window at the top of our browser page to show information to our users.
    // alert("Hello world");

    function showSampleAlert(){
        alert("Hello, User!");
    }
    // showSampleAlert();

// [Section] Prompt
        let samplePrompt = prompt("Enter your Name:");
        console.log(samplePrompt);
        console.log(typeof samplePrompt);

        /* 
        Syntax:
        prompt("<dialoginString>");
        */
       let sampleNullPrompt = prompt("Don't enter anything");
       console.log(sampleNullPrompt);
       console.log(typeof sampleNullPrompt);

       function printWelcomeMessage(){
        let firstName = prompt ("Enter your First Name:");
        let lastName = prompt("Enter your Last Name:");

        console.log("Hello "+firstName+" "+lastName);
       }
       printWelcomeMessage();
    // [Section] Function Naming Convention
        // Function names should be definitive of the task it will perform.
        // it Usually contains a verb
        // e.g
        function getCourse(){
            let course = ["Science101", "Math 101"];
            console.log(course);
        }
        getCourse();
        // Avoid Generic names to aboid confusion within your code/program
        function get(){
            let name="Jamie";
            console.log(name);
        }
        get(); //Instead use getName();

        // avoid pointless function name
        function foo(){
            console.log(25%5);
        }
        foo();

        // Use Smalll caps. followed with camelCasing

        function displayCarInfo($brand){
            console.log("Brand: " + $brand);
        }
        displayCarInfo("Toyota");